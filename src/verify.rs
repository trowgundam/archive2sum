use std::io::BufRead;
use std::ops::Deref;
use std::path::Path;

use md5::{Digest, Md5};
use sha1::Sha1;

use colored::Colorize;

use crate::algorithm::Algorithm;
use futures::future::join_all;
use std::fs::{metadata, File};
use std::io::{copy, BufReader};

use crate::manifest::Manifest;
use crate::settings::Settings;

const KB: usize = 1024;
const MB: usize = 1024 * KB;
const BUFFER_SIZE: usize = 4 * MB;

#[derive(Clone)]
enum VerifyResult {
    CheckSums {
        file_name: String,
        size: Option<bool>,
        crc32: Option<bool>,
        md5: Option<bool>,
        sha1: Option<bool>
    },
    NotFound {
        file_name: String,
    },
    Err {
        file_name: String,
        message: String,
    },
}

pub async fn verify_manifests(settings: &Settings) {
    let algorithms_to_check = Vec::from(settings.algorithms.as_slice());

    for manifest_path in &settings.manifests {
        let path = Path::new(manifest_path.as_str());
        let manifest = Manifest::from_file(path);
        let manifest = match manifest {
            Err(e) => {
                eprintln!("# {}", manifest_path.as_str().bold());
                eprintln!("  - {}", e.as_str().red());
                continue;
            }
            Ok(m) => m,
        };

        let base_path = path.parent().unwrap();

        let mut results = vec![];
        for file in manifest.entries.iter().cloned() {
            if !settings.files.is_empty() & !settings.files.contains(&file.name) {
                continue;
            }

            if !settings.regex.is_empty()
                & !settings
                    .regex
                    .iter()
                    .any(|r| r.is_match(file.name.as_str()))
            {
                continue;
            }

            let mut base_path = base_path.clone().to_path_buf();
            let algorithms = algorithms_to_check
                .iter()
                .cloned()
                .collect::<Vec<Algorithm>>();
            results.push(tokio::spawn(async move {
                base_path.push(file.name.clone());
                let target_path = base_path.deref();

                if !target_path.exists() {
                    return VerifyResult::NotFound { file_name: file.name.clone() };
                }

                let mut size: Option<bool> = None;
                let mut crc32: Option<bool> = None;
                let mut md5: Option<bool> = None;
                let mut sha1: Option<bool> = None;

                for algorithm in algorithms {
                    match algorithm {
                        Algorithm::Size => {
                            let target = match file.size.clone() {
                                None => {
                                    continue;
                                }
                                Some(s) => s.to_string(),
                            };

                            match metadata(target_path) {
                                Err(e) => {
                                    return VerifyResult::Err {
                                        file_name: file.name.clone(),
                                        message: e.to_string()
                                    };
                                }
                                Ok(md) => {
                                    size = Some(md.len().to_string() == target);
                                }
                            }
                        }
                        Algorithm::Crc32 => {
                            let target = match file.crc32.clone() {
                                None => {
                                    continue;
                                }
                                Some(t) => t.clone(),
                            };
                            let mut hasher = crc32fast::Hasher::new();
                            let mut reader = match get_reader(target_path) {
                                Err(e) => {
                                    return VerifyResult::Err {
                                        file_name: file.name.clone(),
                                        message: e
                                    };
                                }
                                Ok(r) => r,
                            };

                            loop {
                                match reader.fill_buf() {
                                    Err(e) => {
                                        return VerifyResult::Err {
                                            file_name: file.name.clone(),
                                            message: e.to_string()
                                        };
                                    }
                                    Ok(b) => match b.len() {
                                        0 => {
                                            break;
                                        }
                                        l => {
                                            hasher.update(b);
                                            reader.consume(l);
                                        }
                                    },
                                }
                            }

                            crc32 = Some(format!("{:x}", hasher.finalize()) == target.as_str());
                        }
                        Algorithm::Md5 => {
                            let target = match file.md5.clone() {
                                None => {
                                    continue;
                                }
                                Some(t) => t.clone(),
                            };
                            let mut hasher = Md5::new();
                            let mut reader = match get_reader(target_path) {
                                Err(e) => {
                                    return VerifyResult::Err {
                                        file_name: file.name.clone(),
                                        message: e };
                                }
                                Ok(r) => r,
                            };
                            md5 = match copy(&mut reader, &mut hasher) {
                                Err(e) => {
                                    return VerifyResult::Err {
                                        file_name: file.name.clone(),
                                        message: e.to_string()
                                    };
                                }
                                Ok(_) => {
                                    Some(format!("{:x}", hasher.finalize()) == target.as_str())
                                }
                            };
                        }
                        Algorithm::Sha1 => {
                            let target = match file.sha1.clone() {
                                None => {
                                    continue;
                                }
                                Some(t) => t.clone(),
                            };
                            let mut hasher = Sha1::new();
                            let mut reader = match get_reader(target_path) {
                                Err(e) => {
                                    return VerifyResult::Err {
                                        file_name: file.name.clone(),
                                        message: e
                                    };
                                }
                                Ok(r) => r,
                            };
                            sha1 = match copy(&mut reader, &mut hasher) {
                                Err(e) => {
                                    return VerifyResult::Err {
                                        file_name: file.name.clone(),
                                        message: e.to_string()
                                    };
                                }
                                Ok(_) => {
                                    Some(format!("{:x}", hasher.finalize()) == target.as_str())
                                }
                            };
                        }
                    }
                }

                return VerifyResult::CheckSums {
                    file_name: file.name.clone(),
                    size,
                    crc32,
                    md5,
                    sha1
                };
            }));
        }
        let results = join_all(results).await;

        let mut _header_printed = false;
        let mut print_header = || {
            if !_header_printed {
                println!("# {}", manifest_path.as_str().bold());
                _header_printed = true;
            }
        };

        for result in results {
            match result {
                Err(e) => {
                    print_header();
                    eprintln!("  - {}", e.to_string().as_str().red());
                    continue;
                }
                Ok(result) => {
                    let file = match result.clone() {
                        VerifyResult::CheckSums { file_name, .. } => file_name,
                        VerifyResult::NotFound { file_name } => file_name,
                        VerifyResult::Err { file_name, .. } => file_name,
                    };
                    let mut _file_header_printed = false;
                    let mut print_file_header = || {
                        if !_file_header_printed {
                            print_header();
                            println!("  - {}", file);
                            print!("    ");
                            _file_header_printed = true;
                        } else {
                            print!(" ");
                        }
                    };

                    match result {
                        VerifyResult::Err { message, ..} => {
                            print_file_header();
                            print!("{}", &message);
                        }
                        VerifyResult::NotFound { .. } => {
                            if !settings.suppress_missing {
                                print_file_header();
                                print!("{}", "File Not Found".red());
                            }
                        }
                        VerifyResult::CheckSums { size, crc32, md5, sha1, .. } => {
                            let has_result =
                                size.is_some() | crc32.is_some() | md5.is_some() | sha1.is_some();
                            if has_result {
                                let mut _file_header_shown = false;
                                for alg in algorithms_to_check.iter().cloned() {
                                    match alg {
                                        Algorithm::Size => match size {
                                            None => {
                                                if !settings.quiet {
                                                    print_file_header();
                                                    print!(
                                                        "{} {} ",
                                                        "Size:".yellow(),
                                                        "N/A".bright_black()
                                                    );
                                                }
                                            }
                                            Some(s) => {
                                                if s && settings.quiet {
                                                    continue;
                                                }

                                                print_file_header();
                                                print!("{} ", "Size:".yellow());
                                                match s {
                                                    true => {
                                                        print!("{}", "Pass".green());
                                                    }
                                                    false => {
                                                        print!("{}", "Fail".red());
                                                    }
                                                }
                                            }
                                        },
                                        Algorithm::Crc32 => match crc32 {
                                            None => {
                                                if !settings.quiet {
                                                    print_file_header();
                                                    print!(
                                                        "{} {} ",
                                                        "CRC32:".yellow(),
                                                        "N/A".bright_black()
                                                    );
                                                }
                                            }
                                            Some(s) => {
                                                if s && settings.quiet {
                                                    continue;
                                                }

                                                print_file_header();
                                                print!("{} ", "CRC32:".yellow());
                                                match s {
                                                    true => {
                                                        print!("{}", "Pass".green());
                                                    }
                                                    false => {
                                                        print!("{}", "Fail".red());
                                                    }
                                                }
                                            }
                                        },
                                        Algorithm::Md5 => match md5 {
                                            None => {
                                                if !settings.quiet {
                                                    print_file_header();
                                                    print!(
                                                        "{} {} ",
                                                        "MD5:".yellow(),
                                                        "N/A".bright_black()
                                                    );
                                                }
                                            }
                                            Some(s) => {
                                                if s && settings.quiet {
                                                    continue;
                                                }

                                                print_file_header();
                                                print!("{} ", "MD5:".yellow());
                                                match s {
                                                    true => {
                                                        print!("{}", "Pass".green());
                                                    }
                                                    false => {
                                                        print!("{}", "Fail".red());
                                                    }
                                                }
                                            }
                                        },
                                        Algorithm::Sha1 => match sha1 {
                                            None => {
                                                if !settings.quiet {
                                                    print_file_header();
                                                    print!(
                                                        "{} {} ",
                                                        "SHA1:".yellow(),
                                                        "N/A".bright_black()
                                                    );
                                                }
                                            }
                                            Some(s) => {
                                                if s && settings.quiet {
                                                    continue;
                                                }

                                                print_file_header();
                                                print!("{} ", "SHA1:".yellow());
                                                match s {
                                                    true => {
                                                        print!("{}", "Pass".green());
                                                    }
                                                    false => {
                                                        print!("{}", "Fail".red());
                                                    }
                                                }
                                            }
                                        },
                                    }
                                }
                            }
                        }
                    }

                    if _file_header_printed {
                        println!();
                    }
                }
            }
        }
    }
}

fn get_reader(path: &Path) -> Result<BufReader<File>, String> {
    match File::open(path) {
        Err(e) => Err(e.to_string()),
        Ok(f) => Ok(BufReader::with_capacity(BUFFER_SIZE, f)),
    }
}
