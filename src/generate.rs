use colored::Colorize;

use crate::algorithm::Algorithm;
use crate::manifest::Manifest;
use crate::settings::Settings;

pub fn generate_sum_file(settings: &Settings) {
    let manifest = Manifest::from_file_path(settings.manifests.first().unwrap());
    let manifest = match manifest {
        Err(e) => {
            eprintln!("{}", e.as_str().red());
            return;
        }
        Ok(m) => m,
    };
    let alg = settings.algorithms.first().unwrap_or(&Algorithm::Sha1);
    for file in manifest.entries {
        let hash = match alg {
            Algorithm::Size => match file.size {
                None => None,
                Some(s) => Some(s.to_string()),
            },
            Algorithm::Crc32 => file.crc32,
            Algorithm::Md5 => file.md5,
            Algorithm::Sha1 => file.sha1,
        };
        if hash.is_some() {
            println!("{}  {}", hash.unwrap().to_string(), file.name.as_str())
        }
    }
}
