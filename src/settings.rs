use std::path::Path;
use std::str::FromStr;

use regex::Regex;

use crate::algorithm::Algorithm;
use crate::mode::Mode;

pub struct Settings {
    pub mode: Mode,
    pub jobs: usize,
    pub algorithms: Vec<Algorithm>,
    pub regex: Vec<Regex>,
    pub files: Vec<String>,
    pub quiet: bool,
    pub clean: bool,
    pub suppress_missing: bool,
    pub manifests: Vec<String>,
}

impl Settings {
    pub fn new() -> Result<Settings, String> {
        let matches = clap_app!(myapp =>
            (name: "archive2sum")
            (version: "0.7.1")
            (author: "Jeffrey Geer <viriveri@gmail.com>")
            (@arg jobs: -j --jobs +takes_value default_value("1") validator(validate_jobs) "Number of threads to use")
            (@subcommand generate =>
                (about: "Generates a check sum file")
                (@arg algorithm: -a --algorithm +takes_value default_value("SHA1") validator(validate_algorithms) "Algorithm to output")
                (@arg MANIFEST: +required validator(validate_file) "File Manifest to Process")
            )
            (@subcommand check =>
                (about: "Checks for extra and missing files")
                (@arg regex: -r --regex ... +takes_value "Regex Pattern of Files to Check")
                (@arg clean: -c --clean "Deletes any extra files")
                (@arg MANIFEST: +required ... validator(validate_file) "File Manifest to Process")
            )
            (@subcommand verify =>
                (about: "Verifies files in File Manifest")
                (@arg algorithm: -a --algorithm ... +takes_value +use_delimiter validator(validate_algorithms) "Algorithm to verify")
                (@arg quiet: -q --quiet "Don't output success messages")
                (@arg suppress_missing: -n --nomissing "Don't output \"File Not Found\" messages")
                (@arg file: -f --file ... +takes_value conflicts_with("regex") "File identifier from Manifest to verify")
                (@arg regex: -r --regex ... +takes_value conflicts_with("file") validator(validate_regex) "Regex Pattern of Files to Verify")
                (@arg MANIFEST: +required ... validator(validate_file) "File Manifest to Process")
            )
        ).get_matches();

        match matches.subcommand() {
            ("generate", Some(sub_m)) => Ok(Settings {
                mode: Mode::Generate,
                jobs: usize::from_str(matches.value_of("jobs").unwrap()).unwrap(),
                algorithms: vec![Algorithm::from_str(sub_m.value_of("algorithm").unwrap()).unwrap()],
                regex: vec![],
                files: vec![],
                quiet: false,
                clean: false,
                suppress_missing: false,
                manifests: vec![sub_m.value_of("MANIFEST").unwrap().to_string()],
            }),
            ("check", Some(sub_m)) => Ok(Settings {
                mode: Mode::Check,
                jobs: usize::from_str(matches.value_of("jobs").unwrap()).unwrap(),
                algorithms: vec![],
                regex: match sub_m.values_of("regex") {
                    None => vec![],
                    Some(re) => re.map(|r| Regex::new(r).unwrap()).collect(),
                },
                files: vec![],
                quiet: false,
                clean: sub_m.values_of("clean").is_some(),
                suppress_missing: false,
                manifests: sub_m
                    .values_of("MANIFEST")
                    .unwrap()
                    .map(|f| f.to_string())
                    .collect(),
            }),
            ("verify", Some(sub_m)) => Ok(Settings {
                mode: Mode::Verify,
                jobs: usize::from_str(matches.value_of("jobs").unwrap()).unwrap(),
                algorithms: match sub_m.values_of("algorithm") {
                    None => Algorithm::values(),
                    Some(a) => a.map(|v| Algorithm::from_str(v).unwrap()).collect(),
                },
                regex: match sub_m.values_of("regex") {
                    None => vec![],
                    Some(re) => re.map(|r| Regex::new(r).unwrap()).collect(),
                },
                files: match sub_m.values_of("file") {
                    None => vec![],
                    Some(fs) => fs.map(|f| f.to_string()).collect(),
                },
                quiet: sub_m.values_of("quiet").is_some(),
                clean: false,
                suppress_missing: sub_m.values_of("suppress_missing").is_some(),
                manifests: sub_m
                    .values_of("MANIFEST")
                    .unwrap()
                    .map(|f| f.to_string())
                    .collect(),
            }),
            _ => Err("No Valid Sub-Command".to_string()),
        }
    }
}

fn validate_jobs(j: String) -> Result<(), String> {
    match usize::from_str(j.as_str()) {
        Err(e) => Err(e.to_string()),
        Ok(j) => match j {
            0 => Err("Job Count must be greater than 0".to_string()),
            _ => Ok(()),
        },
    }
}

fn validate_file(f: String) -> Result<(), String> {
    match Path::new(&f[..]).exists() {
        true => Ok(()),
        false => Err(format!("File Not Found: {}", f)),
    }
}

fn validate_algorithms(a: String) -> Result<(), String> {
    Algorithm::from_str(a.as_str())?;
    Ok(())
}

fn validate_regex(r: String) -> Result<(), String> {
    match Regex::new(r.as_str()) {
        Ok(_) => Ok(()),
        Err(e) => Err(format!("Invalid Regex: {}", e.to_string())),
    }
}
