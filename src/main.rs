#[macro_use]
extern crate clap;
extern crate colored;
extern crate crc32fast;
extern crate futures;
extern crate md5;
extern crate regex;
extern crate sha1;
extern crate tokio;

use colored::*;

use crate::mode::Mode;
use crate::settings::Settings;

mod algorithm;
mod check;
mod generate;
mod manifest;
mod mode;
mod settings;
mod verify;

fn main() {
    let settings = match Settings::new() {
        Ok(s) => s,
        Err(e) => {
            eprintln!("{}", e.as_str().red());
            return;
        }
    };

    let _runtime = tokio::runtime::Builder::new_multi_thread()
        .worker_threads(settings.jobs)
        .enable_all()
        .build()
        .unwrap()
        .block_on(async {
            match settings.mode {
                Mode::Generate => {
                    generate::generate_sum_file(&settings);
                }
                Mode::Check => {
                    check::file_check(&settings).await;
                }
                Mode::Verify => {
                    verify::verify_manifests(&settings).await;
                }
            }
        });
}
