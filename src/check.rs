use std::io;
use std::io::Write;
use std::path::{Path, PathBuf};

use async_recursion::async_recursion;
use colored::Colorize;
use tokio::fs;

use futures::future::join_all;

use crate::manifest::Manifest;
use crate::settings::Settings;

pub async fn file_check(settings: &Settings) {
    for manifest_path in &settings.manifests {
        let mut header_printed = false;
        let mut print_file_header = || {
            if !header_printed {
                println!("# {}", manifest_path);
                header_printed = true;
            }
        };

        let path = Path::new(manifest_path.as_str());
        let manifest = Manifest::from_file(path);
        let manifest = match manifest {
            Err(e) => {
                print_file_header();
                eprintln!("  - {}", e.as_str().red());
                continue;
            }
            Ok(m) => m,
        };

        let base_path = path.parent().unwrap();
        let mut files = match get_files(base_path).await {
            Err(e) => {
                print_file_header();
                eprintln!("  - {}", e);
                vec![]
            }
            Ok(f) => f,
        };

        let mut results = vec![];
        for file in manifest.entries.iter().cloned() {
            if !settings.files.is_empty() & !settings.files.contains(&file.name) {
                continue;
            }

            if !settings.regex.is_empty()
                & !settings
                    .regex
                    .iter()
                    .any(|r| r.is_match(file.name.as_str()))
            {
                continue;
            }

            let mut base_path = base_path.clone().to_path_buf();
            results.push(tokio::spawn(async move {
                base_path.push(file.name.clone());
                return match fs::metadata(base_path.clone()).await {
                    Err(_) => (file.name.clone(), base_path, false),
                    Ok(_) => (file.name.clone(), base_path, true),
                };
            }));
        }
        let results = join_all(results).await;

        let mut missing_header_shown = false;
        for result in results {
            match result {
                Err(_) => {}
                Ok((file, path, exists)) => {
                    if !exists {
                        print_file_header();
                        if !missing_header_shown {
                            println!("## {}", "Missing Files:".red().bold());
                            missing_header_shown = true;
                        }
                        println!("  - {}", file.as_str().red());
                    }

                    match files.iter().position(|f| {
                        same_file::is_same_file(f.clone(), path.clone()).unwrap_or(false)
                    }) {
                        None => {}
                        Some(pos) => {
                            files.remove(pos);
                        }
                    }
                }
            }
        }

        if !files.is_empty() {
            print_file_header();
            println!("## {}", "Extra Files:".green().bold());
            for extra in files.clone() {
                let print_path = pathdiff::diff_paths(&extra, base_path)
                    .unwrap_or(extra.to_path_buf())
                    .to_str()
                    .unwrap_or_default()
                    .to_owned();
                println!("  - {}", print_path.as_str().green());
            }

            if settings.clean {
                print!("Delete the above files? (y/{}) ", "N".bold());
                let _ = io::stdout().flush(); // Gotta do this or the input hits before the print

                let stdin = io::stdin();
                let mut input = String::new();
                match stdin.read_line(&mut input) {
                    Err(_) => {
                        input = "N".to_string();
                    },
                    _ => {
                        input = input[..1].to_string()
                    },
                };

                if input.eq_ignore_ascii_case("y") {
                    print!("Deleting");
                    for extra in files {
                        print!(".");
                        match fs::remove_file(extra).await {
                            Err(e) => {
                                eprintln!("{}", e.to_string().as_str().red());
                                return;
                            },
                            _ => {},
                        }
                    }
                }
            }
        }

        if header_printed {
            println!();
        }
    }
}

#[async_recursion]
pub async fn get_files(path: &Path) -> Result<Vec<PathBuf>, String> {
    let mut entries = match fs::read_dir(path).await {
        Err(e) => {
            return Err(e.to_string());
        }
        Ok(p) => p,
    };

    let mut result = vec![];
    loop {
        let entry = match entries.next_entry().await {
            Err(e) => {
                return Err(e.to_string());
            }
            Ok(None) => {
                break;
            }
            Ok(Some(e)) => e.path(),
        };

        if entry.is_dir() {
            let mut sub_entries = get_files(&entry).await?;
            result.append(&mut sub_entries);
        }

        if entry.is_file() {
            result.push(entry);
        }
    }

    Ok(result)
}
