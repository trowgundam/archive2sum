extern crate xml;

use self::xml::attribute::OwnedAttribute;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::str::FromStr;
use xml::reader::{EventReader, Events, XmlEvent};

#[derive(Clone)]
pub struct Manifest {
    pub entries: Vec<ManifestEntry>,
}

#[derive(Clone)]
pub struct ManifestEntry {
    pub name: String,
    pub size: Option<usize>,
    pub crc32: Option<String>,
    pub md5: Option<String>,
    pub sha1: Option<String>,
}

impl Manifest {
    pub fn from_file_path(file_path: &str) -> Result<Manifest, String> {
        Manifest::from_file(Path::new(file_path))
    }

    pub fn from_file(path: &Path) -> Result<Manifest, String> {
        let file = match File::open(path) {
            Err(e) => {
                return Err(e.to_string());
            }
            Ok(f) => f,
        };
        let file = BufReader::new(file);
        let reader = EventReader::new(file);
        let mut iterator = reader.into_iter();
        loop {
            let xml_event = match iterator.next() {
                None => {
                    return Err("Invalid Xml".to_string());
                }
                Some(Err(e)) => {
                    return Err(e.to_string());
                }
                Some(Ok(xe)) => xe,
            };

            match xml_event {
                XmlEvent::StartElement { name, .. } => {
                    return match name.to_string().as_str() {
                        "files" => Ok(Manifest::from_xml(&mut iterator)?),
                        _ => Err("Unexpected Root Xml Tag".to_string()),
                    };
                }
                _ => {}
            }
        }
    }

    fn from_xml(iterator: &mut Events<BufReader<File>>) -> Result<Manifest, String> {
        let mut entries = vec![];
        loop {
            let xml_event = match iterator.next() {
                None => {
                    return Err("Invalid Xml".to_string());
                }
                Some(Err(e)) => {
                    return Err(e.to_string());
                }
                Some(Ok(xe)) => xe,
            };

            match xml_event {
                XmlEvent::StartElement {
                    name, attributes, ..
                } => match name.to_string().as_str() {
                    "file" => match ManifestEntry::from_xml(attributes.as_slice(), iterator) {
                        Ok(me) => {
                            entries.push(me);
                        }
                        Err(e) => return Err(e.clone()),
                    },
                    _ => {}
                },
                XmlEvent::EndElement { name, .. } => match name.to_string().as_str() {
                    "files" => {
                        entries.sort_by(|a, b| a.name.cmp(&b.name));
                        return Ok(Manifest {
                            entries: entries.clone(),
                        });
                    }
                    _ => {}
                },
                _ => {}
            }
        }
    }
}

impl ManifestEntry {
    fn from_xml(
        attributes: &[OwnedAttribute],
        iterator: &mut Events<BufReader<File>>,
    ) -> Result<ManifestEntry, String> {
        let name = ManifestEntry::find_name(attributes)?;
        let mut size: Option<usize> = None;
        let mut crc32: Option<String> = None;
        let mut md5: Option<String> = None;
        let mut sha1: Option<String> = None;

        loop {
            let xml_event = match iterator.next() {
                None => {
                    return Err("Invalid Xml".to_string());
                }
                Some(Err(e)) => {
                    return Err(e.to_string());
                }
                Some(Ok(xe)) => xe,
            };

            match xml_event {
                XmlEvent::StartElement { name, .. } => match name.to_string().as_str() {
                    "size" => {
                        size = match usize::from_str(ManifestEntry::inner_text(iterator)?.as_str())
                        {
                            Err(e) => {
                                return Err(e.to_string());
                            }
                            Ok(s) => Some(s),
                        };
                    }
                    "crc32" => {
                        crc32 = Some(ManifestEntry::inner_text(iterator)?);
                    }
                    "md5" => {
                        md5 = Some(ManifestEntry::inner_text(iterator)?);
                    }
                    "sha1" => {
                        sha1 = Some(ManifestEntry::inner_text(iterator)?);
                    }
                    _ => {}
                },
                XmlEvent::EndElement { name, .. } => match name.to_string().as_str() {
                    "file" => {
                        break;
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        Ok(ManifestEntry {
            name,
            size,
            crc32,
            md5,
            sha1,
        })
    }

    fn inner_text(iterator: &mut Events<BufReader<File>>) -> Result<String, String> {
        loop {
            let xml_event = match iterator.next() {
                None => {
                    return Err("Invalid Xml".to_string());
                }
                Some(Err(e)) => {
                    return Err(e.to_string());
                }
                Some(Ok(xe)) => xe,
            };

            match xml_event {
                XmlEvent::Characters(s) => {
                    return Ok(s.clone());
                }
                _ => {}
            }
        }
    }

    fn find_name(attributes: &[OwnedAttribute]) -> Result<String, String> {
        for attribute in attributes {
            if attribute.name.local_name.as_str() == "name" {
                return Ok(attribute.value.clone());
            }
        }

        Err("No Name Attribute".to_string())
    }
}
