#[derive(Debug)]
pub enum Mode {
    Generate,
    Check,
    Verify,
}
