use std::fmt::{Display, Formatter};
use std::hash::{Hash, Hasher};
use std::str::FromStr;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Algorithm {
    Size,
    Crc32,
    Md5,
    Sha1,
}

impl Algorithm {
    pub fn values() -> Vec<Algorithm> {
        vec![
            Algorithm::Size,
            Algorithm::Crc32,
            Algorithm::Md5,
            Algorithm::Sha1,
        ]
    }
}

impl Display for Algorithm {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Algorithm::Size => write!(f, "Size"),
            Algorithm::Crc32 => write!(f, "CRC32"),
            Algorithm::Md5 => write!(f, "MD5"),
            Algorithm::Sha1 => write!(f, "SHA1"),
        }
    }
}

impl FromStr for Algorithm {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        for alg in Algorithm::values() {
            if format!("{:?}", alg).to_lowercase() == s.to_lowercase() {
                return Ok(alg);
            }
        }

        Err(format!("Unknown Algorithm: {}", s))
    }
}

impl Hash for Algorithm {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let val = match self {
            Algorithm::Size => 1,
            Algorithm::Crc32 => 2,
            Algorithm::Md5 => 3,
            Algorithm::Sha1 => 4,
        };
        val.hash(state);
    }
}
